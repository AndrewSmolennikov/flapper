//Инициалищация Canvas
const canvas = document.getElementById('myFlapper');
const ctx = canvas.getContext('2d');
//Переменные для AJAX
const key = 'flapperBySmolennikov4';
const url = 'https://fe.it-academy.by/AjaxStringStorage2.php';
const readMethod = 'READ';
const insertMethod = 'INSERT';
const lockGetMethod = 'LOCKGET';
const updateMethod = 'UPDATE';
const name = 'flapper2019';
const pass = 'letmein'
var playerName = '';
//Для браузеров без поддержки localStorage
const locStoreAvailvale = function(){
    var test = 'test';
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return true;
    } catch(e) {
        return false;
    }
}();

//Для поддержки Object.keys
const objKeysComp = function(){
    if(!Object.keys) Object.keys = function(o){
        if (o !== Object(o))
           throw new TypeError('Object.keys called on non-object');
        var ret=[],p;
        for(p in o) if(Object.prototype.hasOwnProperty.call(o,p)) ret.push(p);
        return ret;
     }
}();

vibration = function () {
    if (navigator.vibrate) { 
        window.navigator.vibrate(500); 
    }
}

var leaders = {};
var userName = '';
var bestValue = 0;

//Создание спрайта
const sprite = new Image();
sprite.src = 'image/gameElementsSprite.png';
//Счетчик отрисовок
let frames = 0;

//Состояние игры
const state = {
    current : 0, //Содержит текущее состояние
    getReady : 0, //Состояние до старта 
    game : 1, //Состояние во время игры
    gameOver : 2 //Состояние после окончания игры
}

//Переменная для перевода из радианов в градусы
const fromRadiansValue = Math.PI/180;

//Звуки
const SCORE_S = new Audio();
SCORE_S.src = "audio/sfx_point.wav";

const FLAP = new Audio();
FLAP.src = "audio/sfx_flap.wav";

const HIT = new Audio();
HIT.src = "audio/sfx_hit.wav";

const SWOOSHING = new Audio();
SWOOSHING.src = "audio/sfx_swooshing.wav";

const DIE = new Audio();
DIE.src = "audio/sfx_die.wav";

//Адаптируем requet animation frame
RequestAnimationFrame = 
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function (callback) {
        window.setTimeout(callback, 1000 / 60);
};

//Объект заднего фона
const backgroung = {
    sX : 0,
    sY : 0,
    w : 275,
    h : 226,
    x : 0,
    y : canvas.height - 226,
    dx: 0.1, //Скорость сдивга
    draw : function() {
        //Рисуем фон
        ctx.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        //Рисуем еще раз, так как размер спрайта меньше.
        ctx.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x + this.w, this.y, this.w, this.h);
    },
    update : function() {
        if (state.current === state.game) {
            //Сдвигаем до тех пор, пока свиг не достигнет длины элемента. После этого обнуляем.
            this.x = (this.x - this.dx)%(this.w/2);
        }
    }
}

//Объект переднего фона
const foreground = {
    sX : 276,
    sY : 0,
    w : 224,
    h : 112,
    x : 0,
    y : canvas.height - 112,
    dx: 2, //Скорость сдивга
    draw : function() {
        //Рисуем фон
        ctx.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        //Рисуем еще раз, так как размер спрайта меньше.
        ctx.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x + this.w, this.y, this.w, this.h);
    },
    update : function() {
        if (state.current === state.game) {
            this.x = (this.x - this.dx)%(this.w/2);
        }
    }
}

//Обхект птички
const flapper = {
    radius : 12,
    //Координаты спрата для анимаций птички
    animation : [
        {sX: 276, sY: 112},
        {sX: 276, sY: 139},
        {sX: 276, sY: 164},
        {sX: 276, sY: 139},
    ],
    //Координаты располождения птички
    x : 50,
    y : 150,
    w : 34, 
    h : 26,
    //Параметр - индекс для кадров анимации птички
    frame : 0,
    speed : 0, 
    gravity : 0.25,
    jump : 4.5,
    draw : function() {
        let flapperCoords = this.animation[this.frame];

        //Сохраняем текущие настройки Canvas
        //Затем поворачиваем птичку(и весь Canvas)
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotation);
        ctx.drawImage(sprite, flapperCoords.sX, flapperCoords.sY, this.w, this.h, -this.w/2, -this.h/2, this.w, this.h);
        ctx.restore();
    },
    flap : function() {
        this.speed = -this.jump;
    },
    update : function() {
        //Задаем частоту обновления в зависимости от состояния игры
        //Махать крыльями медленне, до начала игры
        this.period = state.current === state.getReady ? 10 : 5;
        //Вычисляем индекс картинки в зависимости от текущего кадра и частоты обновления
        this.frame += frames%this.period === 0 ? 1 : 0;
        //Сбрасываем индекс к нулю или же задаем следующий индекс картинки для птички    
        this.frame = this.frame%this.animation.length;

        if (state.current === state.getReady) {
            this.rotattion = 0*fromRadiansValue;
            this.y = 150; //Возвращаем птичку в начальную позицию
        }
        else {
            this.speed += this.gravity;
            this.y += this.speed;

            //Проверяем удар об землю и останавливаем анимацию, если нужно
            if (this.y + this.h/2 >= canvas.height - foreground.h) {    
                this.y = canvas.height - foreground.h;
                this.frame = 1;
                if (state.current === state.game) {
                    state.current = state.gameOver;
                    DIE.play();
                    leaders[playerName] = Math.max(score.value, score.best);
                    storeRecords();
                }
            }

            //Если скороть меньше, чем прыжок, значит птица падет иначе взлетает
            if (this.speed >= this.jump){
                this.rotation = 90*fromRadiansValue;
            }
            else {
                this.rotation = -25*fromRadiansValue;
            }
        }
    },
    resetSpeed : function(){
        flapper.speed = 0;
        flapper.rotation = 0*fromRadiansValue;
    }
}
//Объект собщения Get Ready
const getReadyMessage = {
    sX : 0,
    sY : 228,
    w : 174,
    h : 152,
    x : canvas.width/2 - 173/2,
    y : 80,
    
    draw: function(){
        if (state.current === state.getReady){
            ctx.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        }
    }
}
//Объект собщения Game Over
const gameOverMessage = {
    sX : 175,
    sY : 228,
    w : 225,
    h : 202,
    x : canvas.width/2 - 225/2,
    y : 90,
    
    draw: function(){
        if (state.current == state.gameOver){
            ctx.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);   
        }
    }
}

//Обьект труб
const pipes = {
    position : [],
    top : {
        sX : 553,
        sY : 0
    },
    bottom:{
        sX : 502,
        sY : 0
    },
    w : 53,
    h : 400,
    gap : 150,
    maxYPos : -150,
    dx : 2,
    draw : function(){
        for(let i  = 0; i < this.position.length; i++){
            let p = this.position[i];
            
            let topYPos = p.y;
            let bottomYPos = p.y + this.h + this.gap;
            
            // Рисуев верхнюю трубу
            ctx.drawImage(sprite, this.top.sX, this.top.sY, this.w, this.h, p.x, topYPos, this.w, this.h);  
            
            // Рисуев нижнюю трубу
            ctx.drawImage(sprite, this.bottom.sX, this.bottom.sY, this.w, this.h, p.x, bottomYPos, this.w, this.h);  
        }
    },
    update: function(){
        //Не рисовать, если игра остановлена
        if(state.current !== state.game) return;
        
        //Рисуем каждые 100 кадров
        if(frames%100 === 0){
            this.position.push({
                x : canvas.width,
                y : this.maxYPos * ( Math.random() + 1)
            });
        }
        for(let i = 0; i < this.position.length; i++){
            let p = this.position[i];
            
            let bottomPipeYPos = p.y + this.h + this.gap;
            
            // Проверяем столкновение с
            // Верхней трубой
            if(flapper.x + flapper.radius > p.x && flapper.x - flapper.radius < p.x + this.w && flapper.y + flapper.radius > p.y && flapper.y - flapper.radius < p.y + this.h){
                state.current = state.gameOver;
                HIT.play();
                vibration(); //Вибрация при столкновении
                leaders[playerName] = Math.max(score.value, score.best);
                storeRecords();
            }
            // Нижней трубой
            if(flapper.x + flapper.radius > p.x && flapper.x - flapper.radius < p.x + this.w && flapper.y + flapper.radius > bottomPipeYPos && flapper.y - flapper.radius < bottomPipeYPos + this.h){
                state.current = state.gameOver;
                HIT.play();
                vibration(); //Вибрация при столкновении
                leaders[playerName] = Math.max(score.value, score.best);
                storeRecords();
            }
            // Движение
            p.x -= this.dx;

            // Удаляем обекты, которые выходят за пределы canvas и сохраянем очки
            if (p.x + this.w <= 0){
                this.position.shift();
                score.value += 1;
                SCORE_S.play();
                score.best = Math.max(score.value, score.best);
                if (locStoreAvailvale)
                localStorage.setItem(userName, score.best);
                else bestValue = score.best;
            }
        }
    },
    
    reset : function(){
        this.position = [];
    }
}

const startButton = {
    x : 120,
    y : 263,
    w : 83,
    h : 29
}

const score = {
    best: locStoreAvailvale ? parseInt(localStorage.getItem(playerName)) || 0 : bestValue,
    value : 0, 
    draw : function() {
        //Рисуем текущие очки
        ctx.fillStyle = '#FFF';
        ctx.strokeStyle = '#000';
        if (state.current === state.game){
            ctx.lineWidth = 2;
            ctx.font = '35px Teko';
            ctx.fillText(this.value, canvas.width/2, 50);
            ctx.strokeText(this.value, canvas.width/2, 50);
        }
        else if (state.current === state.gameOver) {
            //Рисуем достигнутый результат
            ctx.font = '25px Teko';
            ctx.fillText(this.value, 225, 186);
            ctx.strokeText(this.value, 225, 186);

            //Рисуем лучший результат
            ctx.fillText(this.best, 225, 226);
            ctx.strokeText(this.best, 225, 226);

            //Рисуем тройку лучших игроков
            ctx.font = '20px Teko';
            let keys = Object.keys(leaders);

            keys = keys.sort(function(a, b) {
                return leaders[a] - leaders[b]
            }).reverse();

            let x = 130;
            let y = 175;
            let counters = keys.length <= 3 ? keys.length : 3;
            if (keys.length){
                for(let i = 0; i < counters; i++){
                    ctx.fillText(`${i+1}. ${keys[i]} : ${leaders[keys[i]]}`, x, y);
                    ctx.strokeText(`${i+1}. ${keys[i]} : ${leaders[keys[i]]}`, x, y);
                    y+=22;
                }
            }
        }
    },
    reset : function(){
        score.value = 0;
    }
}

function drawGame() {
    ctx.fillStyle = '#70c5ce';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    backgroung.draw();
    foreground.draw();
    flapper.draw();
    pipes.draw();
    getReadyMessage.draw();
    gameOverMessage.draw();
    score.draw();
}

function updateGame(){
    flapper.update();
    foreground.update();
    backgroung.update();
    pipes.update();
}

function repeater() {
    updateGame();
    drawGame();
    frames++;
    RequestAnimationFrame(repeater);
}

function makeAction(event){
    vibration(); //Вибрация при таче
    switch(state.current){
        case state.getReady:
            state.current = state.game;
            SWOOSHING.play();
            break;
        case state.game:
            if (flapper.y - flapper.radius <= 0) return;
            flapper.flap();
            FLAP.play();
            break;
        case state.gameOver:
            // Выясняем, были клик на кнопку начала игры
            let rect = canvas.getBoundingClientRect();
            let clickX = event.clientX - rect.left;
            let clickY = event.clientY - rect.top;
            
            if(clickX >= startButton.x && clickX <= startButton.x + startButton.w && clickY >= startButton.y && clickY <= startButton.y + startButton.h){
                pipes.reset();
                flapper.resetSpeed();
                score.reset();
                state.current = state.getReady;
            }
        break;
    }
}

function makeTouchAction(event){
    event.preventDefault();
    makeAction(event);
}

function insertNewKey(){
    let defaultHash = {
        'dev1': 15
    };
    
    $.ajax(
        {
            url : url, type : 'POST', cache : false, dataType:'json',
            data : { f : insertMethod, n : key, v: JSON.stringify(defaultHash) }
        }
    );
}

function storeRecords() {
    $.ajax(
        {
            url : url, type : 'POST', cache : false, dataType:'json',
            data : { f : lockGetMethod, n : key, p : pass },
            success: function(data){
                $.ajax({
                    url : url, type : 'POST', cache : false, dataType:'json',
                    data : { f : updateMethod, n : key, v : JSON.stringify(leaders), p : pass }
                });
            }
        }
    );
}

function getRecordsTable(){
    $.ajax(
        {
            url : url, type : 'POST', cache : false, dataType:'json',
            data : { f : readMethod, n : key },
            success : function (data) {
                if(data.result.length) {
                    let hash = JSON.parse(data.result)
                    let keys = Object.keys(hash);
                    let leadersInternal = {};
                    //Сортировка хэша по значениям
                    keys = keys.sort(function(a, b) {
                        return hash[a] - hash[b]
                    }).reverse();

                    for (let i = 0; i < keys.length; i++){
                        leadersInternal[keys[i]] = hash[keys[i]];
                    }
                    leaders = leadersInternal;
                }
                else {
                    insertNewKey();
                }
            }
        }
    );
}

//Обработчик событий canvas
canvas.addEventListener("click", makeAction);
canvas.addEventListener("touchstart", makeTouchAction);

playerName = prompt('Введите имя игрока(3 буквы)');
//Получаем данные рекордов
getRecordsTable();

//Повторитель. Вызывается каждую секунду для перерисовки объектов.
repeater();

